let i = 0;
let first_turm = true;
let summe = 0;
let cell2Summe;
let tbody;

function add_fallturm() {
	i++;
	const table = document.getElementById("order_table");
	if (first_turm) {
		first_turm = false;
		tbody = table.createTBody();
		const tfoot = table.createTFoot();
		const rowSumme = tfoot.insertRow(-1);
		const cell0Summe = rowSumme.insertCell(0)
		rowSumme.insertCell(1);
		cell2Summe = rowSumme.insertCell(2);
		cell0Summe.innerHTML = "<b>Summe</b>";
		document.getElementById("order_cont").style.display = "block";
		document.getElementById("btn_order_confirm").style.display = "inline-block";
	}
	const row = tbody.insertRow(-1);
	const cell0 = row.insertCell(0);
	const cell1 = row.insertCell(1);
	const cell2 = row.insertCell(2);
	cell0.innerHTML = "" + i;
	cell1.innerHTML = "Fallturm " + i;
	cell2.innerHTML = "10000€";
	summe += 10000;
	if (i > 4) {
		cell2Summe.innerHTML = "<b>" + summe * 0.9 + "€</b>";
	} else {
		cell2Summe.innerHTML = "<b>" + summe + "€</b>";
	}
}

function confirmOrder() {
	window.alert("Ein unbekannter Fehler ist beim Verarbeiten der Bestellung aufgetreten. " +
		"Bitte versuchen Sie es in 999999 Tagen erneut.\n" +
		"Oder kontaktieren Sie unseren Kundensupport unter:\n" +
		"HIER KÖNNTE IHRE WERBUNG STEHEN");
}