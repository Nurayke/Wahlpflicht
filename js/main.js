window.onload=function(){
	let input = document.getElementById("search");
	input.addEventListener("keyup", function(event) {
		if (event.key === "Enter") {
			event.preventDefault();
			document.getElementById("search_pressed").click();
		}
	});
}		
		
//Gets input from the search field and opens that website
function redirect() {
	let searchitem = document.getElementById("search");
	let s = searchitem.value + '.html';
	  
	// The following code will only work on a server setup, so we cant check if a website/file exists before opening it
	/*
	var request = new XMLHttpRequest();  
	request.open('GET', s, true);
	request.onreadystatechange = function(){
		if (request.readyState === 4){
			if (request.status === 404) {  
				alert("Oh no, the website doesnt exist!");
			}  
		}
	};
	request.send();
	*/

	//Thats why it is made so that it will always open the page with the search 
	window.location  = s;
}


// Get the modal
let modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target === modal) {
        modal.style.display = "none";
    }
}


// Number of login attempts
let attempt = 3;
// Below function Executes the on click function of the login button
function validate(){
	let username = document.getElementById("username").value;
	let password = document.getElementById("password").value;
	if ( username === "1" && password === "123"){
		alert ("Login successfully");
		window.location = "admin.html"; 
		return false;
	}
	else{
		attempt --;// Decrementing by one.
		alert("Wrong username or password. You have "+attempt+" attempts left.");
		// Disabling fields after 3 attempts.
		if( attempt === 0){
			document.getElementById("username").disabled = true;
			document.getElementById("password").disabled = true;
			document.getElementById("submit").disabled = true;
			return false;
		}
	}
}

function openModal() {
	document.getElementById("myModal").style.display = "block";
}

function closeModal() {
	document.getElementById("myModal").style.display = "none";
}

let slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
	showSlides(slideIndex += n);
}

function currentSlide(n) {
	showSlides(slideIndex = n);
}

function showSlides(n) {
	let i;
	let slides = document.getElementsByClassName("mySlides");
	if (slides.length !== 0) {
		let dots = document.getElementsByClassName("demo");
		let captionText = document.getElementById("caption");
		if (n > slides.length) {slideIndex = 1}
		if (n < 1) {slideIndex = slides.length}
		for (i = 0; i < slides.length; i++) {
			slides[i].style.display = "none";
		}
		for (i = 0; i < dots.length; i++) {
			dots[i].className = dots[i].className.replace(" active", "");
		}
		slides[slideIndex-1].style.display = "block";
		dots[slideIndex-1].className += " active";
		captionText.innerHTML = dots[slideIndex-1].alt;
	}
}