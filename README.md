# Wahlpflicht

Gruppenmitglieder: Tobias Hauhut, Jakob Schwabauer

Verwendete externe Biblioteken:
- Bootstrap 4
- JQuery
- popper.js

Verwendetes JavaScript:
- Navbar (Suche und Login)
- Homepage (index.html, für den Slider)
- Gallery (Anzeige der Bilder in einem Modal)
- Shop (Logik der Anzeige und Berechnung)

Verteilung der Aufgaben

Tobias:
- Homepage, Über uns, Shop, shop.js, css

Jakob:
- Homepage, Über uns, Impressum, Gallery, main.js, css
